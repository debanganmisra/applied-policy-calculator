Applied interview technical test 
For running ,download code and run the sln file.

In program.cs main method comment or uncomment or add new test cases to run the appication.

No UI or input point added currently to the solution.Can be added later if required.

Solution description-----
There are three main classes in the solution
1.Driver-contains driver details name,occupation,dob,claims.

2.Policy-This is the main class for policy calculation.
	Contains following data members and functions.
	1.ListDrivers
	2.PolicyStartDate
	3.Rules-function/algo for implementing policy approval/declination rules.
	4.CalculatePremium-function/algo for calculating premium
	
3.Program-where from the policy class is instantiated and called .