﻿using System;
using System.Collections.Generic;

namespace AppliedInsuranceCalculator
{
    class Driver
    {
        public String Name { set; get; }
        public String Occupation { set; get; }
        public DateTime DOB { set; get; }
        public List<DateTime> Claims { set; get; }
    }
}
