﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppliedInsuranceCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            ////Demo input 1-positive test case 1
            //List<Driver> inputListDrivers = new List<Driver>();
            //List<DateTime> objClaims1 = new List<DateTime>();
            //List<DateTime> objClaims2 = new List<DateTime>();
            //objClaims1.Add(new DateTime(2016, 10, 18));
            //objClaims2.Add(new DateTime(2016, 10, 18));
            //inputListDrivers.Add(new Driver { Name = "Test1", Occupation = "Chaffeur",DOB=new DateTime(1989, 10, 18), Claims = objClaims1 });
            //inputListDrivers.Add(new Driver { Name = "Test2", Occupation = "Accountant", DOB = new DateTime(1989, 10, 18), Claims = objClaims2 });
            //DateTime policyStartDate = new DateTime(2017, 10, 30);

            //Demo input 2-positive test case 2
            List<Driver> inputListDrivers = new List<Driver>();
            List<DateTime> objClaims1 = new List<DateTime>();
            List<DateTime> objClaims2 = new List<DateTime>();
            objClaims1.Add(new DateTime(2016, 10, 18));
            objClaims2.Add(new DateTime(2016, 10, 18));
            objClaims2.Add(new DateTime(2016, 10, 18));
            inputListDrivers.Add(new Driver { Name = "Test1", Occupation = "Accountant", DOB = new DateTime(1987, 10, 18), Claims = objClaims1 });
            inputListDrivers.Add(new Driver { Name = "Test2", Occupation = "Accountant", DOB = new DateTime(1989, 10, 18), Claims = objClaims2 });
            DateTime policyStartDate = new DateTime(2017, 10, 30);

            ////Demo input 3-negative test case 1
            //List<Driver> inputListDrivers = new List<Driver>();
            //List<DateTime> objClaims1 = new List<DateTime>();
            //List<DateTime> objClaims2 = new List<DateTime>();
            //objClaims1.Add(new DateTime(2016, 10, 18));
            //objClaims2.Add(new DateTime(2016, 10, 18));
            //objClaims2.Add(new DateTime(2016, 10, 18));
            //objClaims2.Add(new DateTime(2016, 10, 18));
            //inputListDrivers.Add(new Driver { Name = "Test1", Occupation = "Chaffeur", DOB = new DateTime(1987, 10, 18), Claims = objClaims1 });
            //inputListDrivers.Add(new Driver { Name = "Test2", Occupation = "Accountant", DOB = new DateTime(1989, 10, 18), Claims = objClaims2 });
            //DateTime policyStartDate = new DateTime(2017, 10, 30);

            //Policy object creation
            Policy objPolicy = new Policy();
            objPolicy.ListDrivers = inputListDrivers;
            objPolicy.StartDate = policyStartDate;
            Console.WriteLine("Applied Policy Calculator");
            Console.WriteLine("=========================");
            if (!(objPolicy.ListDrivers.Count > 1 && objPolicy.ListDrivers.Count < 5))
            {
                Console.WriteLine("Input correct details list of drivers for the policy to calculate");
            }
            else if (objPolicy.StartDate==null)
            {
                Console.WriteLine("Input start date and list of drivers for the policy to calculate");
            }
            else
            {
                //Calling Calculate premium function
                Console.WriteLine(objPolicy.CalculatePremium());
            }

            Console.Read();
        }
    }
}
