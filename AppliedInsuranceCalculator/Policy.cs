﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AppliedInsuranceCalculator
{
    class Policy
    {
        #region policy data members
        public DateTime StartDate { set; get; }
        public List<Driver> ListDrivers { set; get; }
        # endregion

        /// <summary>
        /// Rules for policy declination/approval 
        /// </summary>
        /// <returns></returns>
        private string Rules()
        {
            string result = "Approved";
            var youngestDriver = (ListDrivers.OrderBy(d => d.DOB).Select(d=>d).Take(1)).ToList();
            Driver objYoungestDriver = (Driver)youngestDriver[0];
            int YoungestAge = StartDate.Year - objYoungestDriver.DOB.Year;

            if (StartDate.Month < objYoungestDriver.DOB.Month || (StartDate.Month == objYoungestDriver.DOB.Month && StartDate.Day < objYoungestDriver.DOB.Day))
                YoungestAge--;

            var oldestDriver = (ListDrivers.OrderByDescending(d => d.DOB).Select(d => d).Take(1)).ToList();
            Driver objOldestDriver = (Driver)oldestDriver[0];
            int OldestAge = StartDate.Year - objOldestDriver.DOB.Year;

            if (StartDate.Month < objOldestDriver.DOB.Month || (StartDate.Month == objOldestDriver.DOB.Month && StartDate.Day < objOldestDriver.DOB.Day))
                OldestAge--;

            if (StartDate.Date < DateTime.Now.Date)
            {
                result = "Start Day of the policy is in past!!!!";
            }
            else if (YoungestAge < 21)
            {
                result = "Age of the youngest driver" + objYoungestDriver.Name + " is " + YoungestAge;
            }
            else if (OldestAge > 75)
            {
                result = "Age of the oldest driver" + objOldestDriver.Name + "is" + OldestAge;
            }
            foreach (Driver objDriver in ListDrivers)
            {
                if (objDriver.Claims.Count > 2)
                {
                    result = "Driver," + objDriver.Name + " has more than 2 claims";
                    break;
                }
                else if (objDriver.Claims.Count > 3)
                {
                    result = "Driver," + objDriver.Name + " has more than 3 claims";
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// Premium calculation function
        /// </summary>
        /// <returns></returns>
        public string CalculatePremium()
        {
            //Starting premium point
            int premium = 500;

            if (Rules() == "Approved")
            {
                var youngestDriver = (ListDrivers.OrderBy(d => d.DOB).Select(d => d).Take(1)).ToList();
                Driver objYoungestDriver = (Driver)youngestDriver[0];
                int YoungestAge = StartDate.Year - objYoungestDriver.DOB.Year;

                if (StartDate.Month < objYoungestDriver.DOB.Month || (StartDate.Month == objYoungestDriver.DOB.Month && StartDate.Day < objYoungestDriver.DOB.Day))
                    YoungestAge--;

                foreach (Driver objDriver in ListDrivers)
                {
                    if (objDriver.Occupation == "Chaffeur")
                    {
                        premium += Convert.ToInt32(premium * 0.10);
                    }
                    else if (objDriver.Occupation == "Accountant")
                    {
                        premium -= Convert.ToInt32(premium * 0.10);
                    }

                    foreach (DateTime dtClaim in objDriver.Claims)
                    {
                        if (dtClaim > StartDate.AddYears(-1))
                        {
                            premium += Convert.ToInt32(premium * 0.20);
                        }
                        else if (dtClaim < StartDate.AddYears(-1) && dtClaim > StartDate.AddYears(-5))
                        {
                            premium += Convert.ToInt32(premium * 0.10);
                        }
                    }
                }
                
                if (YoungestAge >= 21 && YoungestAge <= 25)
                {
                    premium += Convert.ToInt32(premium * 0.20);
                }
                else if (YoungestAge >= 26 && YoungestAge <= 75)
                {
                    premium -= Convert.ToInt32(premium * 0.10);
                }

                return "The premium is " + premium;
            }
            else
            {
                return Rules();
            }            
        }
    }
}
